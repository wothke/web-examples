# web-examples
Just a collection of little Web related examples and utilites (see sub-folders for
additional information):

- resource_compression: hack to compress arbitrary resource files that you load from JavaScript

- soundcloud:	howto play music from soundcloud.com

- XMP_player: 	howto play music with my XMP player (or any of my other emulators)
		and automatically generate respective playlists  


